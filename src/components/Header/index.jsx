import styled from 'styled-components';
import darkLogo from '../../assets/dark-logo.png';
import StyledLink from '../../utils/style/styledLink';

const NavBar = styled.nav({
  display: 'flex',
  justifyContent: 'space-between',
  paddingInline: '5%',
  alignItems: 'center',
  marginBottom: '30px',
});
const Logo = styled.img({
  height: '70px',
});
function Header() {
  return (
    <NavBar>
      <Logo src={darkLogo}></Logo>
      <div>
        <StyledLink to="/">Accueil</StyledLink>
        <StyledLink to="/freelances">Profils</StyledLink>
        <StyledLink to="/survey/1" $isFullLink>
          Questionnaire
        </StyledLink>
      </div>
    </NavBar>
  );
}
export default Header;

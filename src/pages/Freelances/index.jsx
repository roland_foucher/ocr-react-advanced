import styled from 'styled-components';
import Card from '../../components/Card';

const freelanceProfiles = [
  {
    name: 'Jane Doe',
    jobTitle: 'Devops',
  },
  {
    name: 'John Doe',
    jobTitle: 'Developpeur frontend',
  },
  {
    name: 'Jeanne Biche',
    jobTitle: 'Développeuse Fullstack',
  },
  {
    jobTitle: 'test',
  },
];

const CardsContainer = styled.div`
  display: grid;
  gap: 24px;
  grid-template-rows: 350px 350px;
  grid-template-columns: repeat(2, 1fr);
`;

function Freelances() {
  return (
    <CardsContainer>
      <h1>freelance</h1>
      {freelanceProfiles.map((profile, index) => (
        <Card
          key={`${profile.name} - ${index}`}
          label={profile.jobTitle}
          title={profile.name}
        />
      ))}
    </CardsContainer>
  );
}
export default Freelances;

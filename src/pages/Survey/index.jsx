import { Link, useParams } from 'react-router-dom';

function Survey() {
  const { questionNumber } = useParams();
  const questionNumberInt = parseInt(questionNumber);
  const prevQuestion = questionNumberInt === 1 ? 1 : questionNumberInt - 1;
  const nextQuestion = questionNumberInt + 1;
  return (
    <main>
      <h1>Questionnaire</h1>
      <h2>Question {questionNumber}</h2>
      <Link to={`/survey/${prevQuestion}`}>Precedent</Link>
      {questionNumberInt === 10 ? (
        <Link to={'/results'}>Resultats</Link>
      ) : (
        <Link to={`/survey/${nextQuestion}`}>Suivant</Link>
      )}
    </main>
  );
}
export default Survey;

import homeIllustration from '../../assets/home-illustration.svg';
import styled from 'styled-components';
import colors from '../../utils/style/colors';
import StyledLink from '../../utils/style/styledLink';

const Main = styled.main({
  backgroundColor: colors.backgroundLight,
  display: 'flex',
  maxWidth: '1200px',
  margin: 'auto',
  padding: '60px 90px',
});
const LeftPart = styled.div({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  flex: '1',
});
const StyledTitle = styled.h2({
  paddingBottom: '30px',
  maxWidth: '280px',
  lineHeight: '50px',
});

function Home() {
  return (
    <Main>
      <LeftPart>
        <StyledTitle>
          Repérez vos besoins, on s’occupe du reste, avec les meilleurs talents
        </StyledTitle>
        <div>
          <StyledLink to="/survey/1" $isFullLink>
            Faire le test
          </StyledLink>
        </div>
      </LeftPart>

      <img src={homeIllustration} alt="" />
    </Main>
  );
}

export default Home;

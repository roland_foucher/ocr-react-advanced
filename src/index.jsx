import React from 'react';
import ReactDOM from 'react-dom';
import Home from './pages/Home';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Survey from './pages/Survey';
import Header from './components/Header';
import Error from './components/Error';
import Results from './pages/Results';
import Freelances from './pages/Freelances';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    div {
        font-family: 'Trebuchet MS', Helvetica, sans-serif;
        
    }
`;
ReactDOM.render(
  <Router>
    <GlobalStyle />
    <Header />
    <Routes>
      <Route exact path="/" element={<Home />} />
      <Route path="/survey/:questionNumber" element={<Survey />} />
      <Route path="/results" element={<Results />}></Route>
      <Route path="/freelances" element={<Freelances />}></Route>
      <Route path="*" element={<Error />} />
    </Routes>
  </Router>,
  document.getElementById('root')
);
